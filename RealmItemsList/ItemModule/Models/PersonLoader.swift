//
//  PersonLoader.swift
//  RealmItemsList
//
//  Created by Artur Kvaratshelia on 9/4/19.
//  Copyright © 2019 Artur Kvaratshelia. All rights reserved.
//

import Foundation

class PersonLoader {
    func loadPersons() -> [Person] {
        guard let jsonObject = JSONLoader.jsonObject(from: .persons) else {
            fatalError("Couldn't find json object")
        }
        
        guard let persons = try? JSONParser.array(item: Person.self, jsonObject: jsonObject, key: "persons") else {
            fatalError("Couldn't parse json object")
        }
        
        
        return persons
    }
}
