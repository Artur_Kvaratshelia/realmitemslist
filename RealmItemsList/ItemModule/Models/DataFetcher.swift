//
//  DataFetcher.swift
//  RealmItemsList
//
//  Created by Artur Kvaratshelia on 9/4/19.
//  Copyright © 2019 Artur Kvaratshelia. All rights reserved.
//

import Foundation
import RealmSwift

class DataFetcher {
    
    typealias Completion = (() -> Void)
    
    private let personFetcher = PersonLoader()
    private let groupFetcher = GroupLoader()
    
    func loadData(completion: @escaping Completion) {
        DispatchQueue.global().async { [weak self] in
            self?.fetchData(completion: completion)
        }
    }
    
    private func fetchData(completion: Completion) {
        let persons = personFetcher.loadPersons()
        let groups = groupFetcher.loadGroups()
        
        let realm = try! Realm()
        try? realm.write {
            realm.add(persons, update: true)
            realm.add(groups, update: true)
            completion()
        }
    }
}
