//
//  GroupLoader.swift
//  RealmItemsList
//
//  Created by Artur Kvaratshelia on 9/4/19.
//  Copyright © 2019 Artur Kvaratshelia. All rights reserved.
//

import Foundation

class GroupLoader {
    func loadGroups() -> [Group] {
        guard let jsonObject = JSONLoader.jsonObject(from: .groups) else {
            fatalError("Couldn't find json object")
        }
        
        guard let groups = try? JSONParser.array(item: Group.self, jsonObject: jsonObject, key: "groups") else {
            fatalError("Couldn't parse json object")
        }
        
        return groups
    }
}
