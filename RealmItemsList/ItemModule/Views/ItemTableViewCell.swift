//
//  ItemTableViewCell.swift
//  RealmItemsList
//
//  Created by Artur Kvaratshelia on 9/4/19.
//  Copyright © 2019 Artur Kvaratshelia. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    
    @IBOutlet private var titleLabel: UILabel!
    
    func configure(item: ItemViewModel) {
        titleLabel.text = item.title
    }
}
