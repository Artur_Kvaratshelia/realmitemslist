//
//  ItemsDataSource.swift
//  RealmItemsList
//
//  Created by Artur Kvaratshelia on 9/4/19.
//  Copyright © 2019 Artur Kvaratshelia. All rights reserved.
//

import UIKit

class ItemsDataSource: NSObject {
    
    private let tableView: UITableView
    private let cellName = "ItemTableViewCell"
    private var items: [ItemViewModel] = []
    
    init(tableView: UITableView) {
        self.tableView = tableView
        super.init()
        tableView.dataSource = self
        tableView.register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
    }
    
    func insert(items: [ItemViewModel]) {
        self.items.append(contentsOf: items)
        tableView.reloadData()
    }
}

extension ItemsDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath) as! ItemTableViewCell
        cell.configure(item: item)
        return cell
    }
}
