//
//  ItemViewModel.swift
//  RealmItemsList
//
//  Created by Artur Kvaratshelia on 9/4/19.
//  Copyright © 2019 Artur Kvaratshelia. All rights reserved.
//

import Foundation

protocol ItemViewModel {
    var identifier: Int64 { get }
    var title: String { get }
}
