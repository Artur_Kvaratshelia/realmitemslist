//
//  GroupViewModel.swift
//  RealmItemsList
//
//  Created by Artur Kvaratshelia on 9/4/19.
//  Copyright © 2019 Artur Kvaratshelia. All rights reserved.
//

import Foundation

class GroupViewModel: ItemViewModel {
    
    private let model: Group
    
    init(model: Group) {
        self.model = model
    }
    
    var identifier: Int64 {
        return model.identifier
    }
    
    var title: String {
        return model.name
    }
}
