//
//  PersonViewModel.swift
//  RealmItemsList
//
//  Created by Artur Kvaratshelia on 9/4/19.
//  Copyright © 2019 Artur Kvaratshelia. All rights reserved.
//

import Foundation

class PersonViewModel: ItemViewModel {
    
    private let model: Person
    
    init(model: Person) {
        self.model = model
    }
    
    var identifier: Int64 {
        return model.identifier
    }
    
    var title: String {
        return model.fullName
    }
}
