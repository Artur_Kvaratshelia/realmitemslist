//
//  ItemsViewController.swift
//  RealmItemsList
//
//  Created by Artur Kvaratshelia on 9/3/19.
//  Copyright © 2019 Artur Kvaratshelia. All rights reserved.
//

import UIKit
import RealmSwift

class ItemsViewController: UIViewController {

    @IBOutlet private var tableView: UITableView!
    
    private let dataFetcher = DataFetcher()
    private var dataSource: ItemsDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = ItemsDataSource(tableView: tableView)
        dataFetcher.loadData(completion: { [weak self] in
            DispatchQueue.main.async {
                self?.updateContent()
            }
        })
    }
    
    private func updateContent() {
        let realm = try! Realm()
        let persons = Array(realm.objects(Person.self)).compactMap({ PersonViewModel(model: $0) })
        let groups = Array(realm.objects(Group.self)).compactMap({ GroupViewModel(model: $0) })
        var items: [ItemViewModel] = persons
        items.append(contentsOf: groups)
        items.sort(by: { $0.title < $1.title })
        dataSource.insert(items: items)
    }
}
