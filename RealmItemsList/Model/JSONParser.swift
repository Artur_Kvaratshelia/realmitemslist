//
//  JSONParser.swift
//  RealmItemsList
//
//  Created by Artur Kvaratshelia on 9/3/19.
//  Copyright © 2019 Artur Kvaratshelia. All rights reserved.
//

import Foundation

enum JSONParserError: Error {
    case nestedObjectNotFound
}

class JSONParser {
    static func array<Item: Decodable>(item: Item.Type, jsonObject: JSONObject, key: String) throws -> [Item] {
        if let objects = jsonObject[key] as? [JSONObject] {
            let decoder = JSONDecoder()
            return try objects.compactMap({ try object(item: item, jsonObject: $0, decoder: decoder) })
        } else {
            throw(JSONParserError.nestedObjectNotFound)
        }
    }
    
    static func object<Item: Decodable>(item: Item.Type, jsonObject: JSONObject, decoder: JSONDecoder = JSONDecoder()) throws -> Item {
        let data = try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
        let object = try decoder.decode(item, from: data)
        return object
    }
}
