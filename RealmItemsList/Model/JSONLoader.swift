//
//  JSONLoader.swift
//  RealmItemsList
//
//  Created by Artur Kvaratshelia on 9/3/19.
//  Copyright © 2019 Artur Kvaratshelia. All rights reserved.
//

import Foundation

typealias JSONObject = Dictionary<String, Any>

enum JSONFile: String {
    case persons = "Persons"
    case groups = "Groups"
}

class JSONLoader {
    static func jsonObject(from jsonFile: JSONFile) -> JSONObject? {
        guard let filePath = Bundle(for: self).path(forResource: jsonFile.rawValue, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: filePath), options: .uncached) else {
                return nil
        }
        let result = try? JSONSerialization.jsonObject(with: data, options: .init(rawValue: 0))
        return result as? JSONObject
    }
}
