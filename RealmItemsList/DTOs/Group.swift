//
//  Group.swift
//  RealmItemsList
//
//  Created by Artur Kvaratshelia on 9/3/19.
//  Copyright © 2019 Artur Kvaratshelia. All rights reserved.
//

import Foundation
import RealmSwift

class Group: Object, Codable {
    @objc dynamic var identifier: Int64
    @objc dynamic var name: String
    
    override static func primaryKey() -> String? {
        return "identifier"
    }
}
